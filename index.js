import { ApolloServer, gql } from "apollo-server";
import axios from "axios";

const RickAndMortyAPI = Object.freeze({
  CHARACTER: "https://rickandmortyapi.com/api/character/",
  EPISODE: "https://rickandmortyapi.com/api/episode/",
  LOCATION: "https://rickandmortyapi.com/api/location/",
});

// Defining Data
const typeDefs = gql`
  type Location {
    id: Int!
    name: String!
    type: String!
    dimension: String!
    url: String!
    created: String!
  }

  type Episode {
    id: Int!
    name: String!
    air_date: String!
    episode: String!
    url: String!
    created: String!
  }

  type Character {
    id: Int!
    name: String!
    status: String!
    species: String!
    type: String!
    gender: String!
    origin: Location!
    location: Location!
    image: String!
    episode: [Episode]!
    url: String!
    created: String!
  }

  type Query {
    allCharacters: [Character]!
    Character(id: Int!): Character!

    allLocations: [Location]!
    Location(id: Int!): Location!

    allEpisodes: [Episode]!
    Episode(id: Int!): Episode
  }
`;

// Creating Resolvers
const resolvers = {
  Query: {
    allCharacters: async (root, args) => {
      const { data: CharactersList } = await axios.get(
        RickAndMortyAPI.CHARACTER
      );
      return CharactersList.results;
    },

    Character: async (root, args) => {
      const { data: Chatacter } = await axios.get(
        `${RickAndMortyAPI.CHARACTER}${args.id}`
      );
      return Chatacter;
    },

    allLocations: async (root, args) => {
      const { data: LocationsList } = await axios.get(RickAndMortyAPI.LOCATION);
      return LocationsList.results;
    },

    Location: async (root, args) => {
      const { data: Location } = await axios.get(
        `${RickAndMortyAPI.LOCATION}${args.id}`
      );
      return Location;
    },

    allEpisodes: async (root, args) => {
      const { data: EpisodesList } = await axios.get(RickAndMortyAPI.EPISODE);
      return EpisodesList.results;
    },

    Episode: async (root, args) => {
      const { data: Episode } = await axios.get(
        `${RickAndMortyAPI.EPISODE}${args.id}`
      );
      return Episode;
    },
  },
};

// Creating Server
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

// Starting Server
server.listen().then(({ url }) => {
  console.info(`Server ready at ${url}`);
});
